@component('mail::message')

<h1>{{$user->name}} has created a job named "{{$job->title}}"</h1>
The job description:
{{$job->description}}

@component('mail::button', ['url' => $approveUrl])
    Approve
@endcomponent

@component('mail::button', ['url' => $spamUrl])
    Mark As Spam
@endcomponent

@endcomponent