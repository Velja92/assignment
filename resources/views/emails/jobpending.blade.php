@component('mail::message')

    <h1> Hi {{$user->name}}, </h1>
    Your job {{$job->title}} is pending for a moderation.

@endcomponent