@extends ('layouts.app')

@section ('content')
    <div class="container">
        <div class="row">
            @if(count($jobs))
                <table class="table">
                    <thead>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Date</th>
                    </thead>
                    <tbody>
                        @foreach($jobs as $job)
                            <tr>
                                <td>{{$job->title}}</td>
                                <td>{{$job->author()}}</td>
                                <td>{{$job->created_at}}</td>
                                <td>
                                    <form method="GET" action="/approve/{{$job->id}}">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-primary">
                                            Approve
                                        </button>
                                    </form>
                                </td>
                                <td>
                                    <form method="GET" action="/spam/{{$job->id}}">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-primary">
                                            Spam
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <h2>There are no pending jobs waiting for approval!</h2>
            @endif
        </div>
    </div>

@endsection