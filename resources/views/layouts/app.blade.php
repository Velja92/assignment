@include ('layouts.parts.header')

@include ('layouts.parts.navigation')

@yield ('content')

@include ('layouts.parts.footer')