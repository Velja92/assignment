@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="text-center">Assignment</h1>
            @if(count($jobs))
                @foreach($jobs as $job)
                    <h3>{{$job->title}}</h3>
                    <h6>{{$job->author()}}</h6>
                    <p>{{$job->description}}</p>
                    <hr>
                @endforeach
            @else
                <h3 class="text-center">Currently there are no published posts! </h3>
            @endif
        </div>
    </div>
</div>
@endsection
