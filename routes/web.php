<?php
Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create', 'JobController@create');
Route::post('/store', 'JobController@store')->name('store');
Route::get('/pending', 'JobController@pending')->middleware('moderator');
Route::post('/approve', 'JobController@approve')->name('approve')->middleware('moderator');
Route::get('/approve/{job}', 'JobController@approve')->middleware('moderator');
Route::get('/spam/{job}', 'JobController@spam')->middleware('moderator');
