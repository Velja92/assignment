<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Job;
use App\Mail\JobCreated;
use App\Mail\JobPending;
use App\Mail\JobWaitingForApproval;
use Mockery\Exception;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Create(){
        return view('job.create');
    }

    protected function store(){
        $job = new Job();

        $this->validate(request(), [
            'title' => 'required|string|max:255|unique:jobs,title',
            'email' => 'required|string|email|max:255',
            'description' => 'required|string|min:50|max:4000',
            'user_id' => 'required'
        ]);

        $data = \request()->toArray();
        $user = User::where('id', '=', $data['user_id'])->first();
        $moderator = User::where('role', '=', 'moderator')->first();

        if($job->numberOfPosts($data['user_id']) > 0){
            $status = 'published';
        }
        else{
            $status = 'pending';
        }
        $newJob = Job::create([
            'title' => $data['title'],
            'email' => $data['email'],
            'description' => $data['description'],
            'user_id'  => $data['user_id'],
            'status'  => $status,
        ]);
        if($status == 'pending'){
            \Mail::to($user)->send(new JobPending($user, $newJob));
            if($moderator) {
                \Mail::to($moderator)->send(new JobWaitingForApproval($user, $newJob));
            }
            $message = 'The job was submitted for a review.';
        }
        else{
            $message = 'The job successfully published,';
        }

        return view('job.create', compact('message'));
    }

    public function pending(){

        $jobs = Job::where('status', '=', 'pending')->get();
        return view('job.pending', compact('jobs'));
    }

    public function approve($job){
        Job::where('id', '=', $job)->update(['status' => 'published']);
        return redirect('/pending');
    }

    public function spam($job){
        Job::where('id', '=', $job)->update(['status' => 'spam']);
        return redirect('/pending');
    }
}
