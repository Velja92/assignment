<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Job;
use Illuminate\Support\Facades\URL;

class JobWaitingForApproval extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $job;
    public $approveUrl;
    public $spamUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $job)
    {
        $this->user = $user;
        $this->job = $job;
        $this->approveUrl = URL::to('/approve/' . $job->id);
        $this->spamUrl = URL::to('/spam/' . $job->id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.jobwaitingforapproval');
    }
}
