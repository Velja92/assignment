<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'title', 'email', 'description', 'user_id', 'status'
    ];

    public function author(){
        $user = User::where('id', '=', $this->user_id)->first();
        return $user->name;
    }

    public function numberOfPosts($authorId){
        return Job::where('user_id', '=', $authorId)
            ->where('status', '=', 'published')
            ->count();
    }

}
